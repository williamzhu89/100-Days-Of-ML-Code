# 数据预处理
![Day 1](https://gitee.com/yangmiao123/100-Days-Of-ML-Code/raw/master/info/Day1.jpg)

如图所示，通过6步完成数据预处理

此例用到的[数据](https://gitee.com/yangmiao123/100-Days-Of-ML-Code/tree/master/datasets/Day1.csv)，[代码](https://gitee.com/yangmiao123/100-Days-Of-ML-Code/tree/master/code/Day1/Day1.py)。

## 第1步：导入库
```Python
import numpy as np
import pandas as pd
```
## 第2步：导入数据集
```python
dataset = pd.read_csv('Day1.csv')
X = dataset.iloc[ : , :-1].values
Y = dataset.iloc[ : , 3].values
```
## 第3步：处理丢失数据
```python
from sklearn.preprocessing import Imputer
imputer = Imputer(missing_values = "NaN", strategy = "mean", axis = 0)
imputer = imputer.fit(X[ : , 1:3])
X[ : , 1:3] = imputer.transform(X[ : , 1:3])
```
## 第4步：解析分类数据
```python
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder_X = LabelEncoder()
X[ : , 0] = labelencoder_X.fit_transform(X[ : , 0])
```
### 创建虚拟变量
```python
onehotencoder = OneHotEncoder(categorical_features = [0])
X = onehotencoder.fit_transform(X).toarray()
labelencoder_Y = LabelEncoder()
Y =  labelencoder_Y.fit_transform(Y)
```
## 第5步：拆分数据集为训练集合和测试集合
```python
from sklearn.model_selection import train_test_split
X_train, X_test, Y_train, Y_test = train_test_split( X , Y , test_size = 0.2, random_state = 0)
```
## 第6步：特征量化
```python
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)
```
