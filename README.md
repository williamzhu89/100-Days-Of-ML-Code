# 100-Days-Of-ML-Code
原文：[Avik-Jain：100-Days-Of-ML-Code](https://github.com/Avik-Jain/100-Days-Of-ML-Code)
#### 介绍
机器学习100天

#### **第一天 | 数据处理** 
1、导入需要的库
numpy 和 pandas
numpy-包含数学计算函数
pandas-用于导入和管理数据集

2、导入数据集
数据集采用.csv格式。csv以文本形式保存表格数据，即文件每一行是一条数据记录。可以采用pandas的read_csv读取本地的csv为一个数据帧，通过数据帧制作自变量和因变量的矩阵和向量。

3、处理丢失数据
我们得到的数据很少是完整的，可能由于某种原因造成丢失，为了不降低机器学习性能，需要处理数据。采用sklearn.preprocesing库导入Imputer类

4、解析分类数据
分类数据指含有标签值得变量，取值范围一般是固定的，例如“yes”和“no”不能作为模型的数学计算，需要解析成数字。为了实现这一功能可以从sklearn.perprocesing库导入LabelEncoder类

5、拆分数据集为测试数据集合和训练集合
把数据集处理拆分成两个：一个是用于训练模型的训练集合，一个是用于验证模型的测试集合，两者比例一般为80:20。采用sklearn.crossvalidation库中的train_test_split()方法

6、特征缩放
大部分模型算法使用两点间的欧式距离表示，但此特征在幅度、单位和范围姿态等问题上变化很大。在距离计算中，高幅度的特征比低幅度的特征权重更大，可使用特征标准化或者Z值归一化解决。导入sklearn.preprocrssing库的dardScalar类

#### **第二天 | 简单线性规划**
##### 使用单一特征来预测因变量
这是一种基于自变量(x)来预测因变量(y)的方法。假设这两个变量是线性相关的，我们尝试寻找一种根据特征或者自变量(x)的线性关系来精确预测因变量(y)。
##### 找到最佳的拟合线
在回归任务中，我们将通过“最佳拟合线”来最小化预测的误差，即回归线的误差是最小的，尽量最小化实际值(Yi)和模型预测值(Yp)之间的长度。
<a href="https://www.codecogs.com/eqnedit.php?latex=min\left&space;\{&space;sum(y_i-y_p)^2&space;\right&space;\}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?min\left&space;\{&space;sum(y_i-y_p)^2&space;\right&space;\}" title="min\left \{sum(y_i-y_p)^2 \right \}" /></a>  
<a href="https://www.codecogs.com/eqnedit.php?latex=y=b_0&plus;b_1x_1" target="_blank"><img src="https://images.gitee.com/uploads/images/2019/0110/110407_07fdb02e_1771923.gif" title="y=b_0+b_1x_1" /></a>  
y位因变量，x1为自变量，在这回归任务中，我们将预测一个学生根据所学习小时数来计算分数的百分比。  
<a href="https://www.codecogs.com/eqnedit.php?latex=Score=b_0&plus;b_1*hours" target="_blank"><img src="https://images.gitee.com/uploads/images/2019/0110/110642_433c8c9d_1771923.gif" title="Score=b_0+b_1*hours" /></a>  
b0为y的截距，b1为斜率  
##### 1、数据预处理  
按照之前的数据预处理信息表那样执行相同的步骤
* 导入相关库
* 导入数据集
* 检查缺失数据
* 划分数据集
* 特征缩放(使用线性模型的相关库)

##### 2、训练简单线性回归模型  
为了使用数据集训练模型，我们使用sklearn.liner_model库的LinearRegression类，创建一个LinearRegression类的regressor对象，使用LinearRegression类的fit()方法对数据集进行训练  
##### 3、预测结果  
预测测试集的结果，将输出保存在向量Y_pred中，使用前一步训练的回归模型，采用LinearRegression类预测方法对结果进行预测  
##### 4、可视化  
采用matplotlib.pyplot库对训练结果和预测结果做散点图，查看模型效果  

#### **第三天 | 多元线性回归**
> 多元线性回归尝试通过一种线性方程适配观测数据，这个方程是由两个或者两个以上特征与应变量构建的一个关系。多元线性回归和简单线性回归实现步骤很相似，在评价部分有所不同。你可以用它来找出在预测结果上那个因素影响力最大，以及不同变量是如何互相关联的。
> <a href="https://www.codecogs.com/eqnedit.php?latex=y=b_0&plus;b_1x_1&plus;b_2x_2......b_nx_n" target="_blank"><img src="https://images.gitee.com/uploads/images/2019/0111/144157_7d4ea432_1771923.gif" title="y=b_0+b_1x_1+b_2x_2......b_nx_n" /></a>
> y是因变量，x0,x1...xn是多元自变量
##### 前提
> 想要有一个成功的回归分析，确认这些假设很重要
1. 线性：自变量和因变量的关系应该是线性的(即，特征值和预测值是线性相关的)
2. 方差齐性(常数方差)：误差项的分散(即方差)必须等同
3. 多元正态分布：多元回归假定残差符合正态分布
4. 缺少多重共线性：假设数据有极少甚至没有多重共线性，当特征(或自变量)不是相互独立时，会引发多重共线性
##### 注意
> 过多的变量可能会降低模型的精准度，尤其是如果存在一些对结果无关的变量，或者存在对其他变量造成很大影响的变量。 

**这里介绍一些选择合适变量的方法**  
1. 向前选择法
2. 向后选择法(向后剔除法/向后消元法)
3. 向前向后法：结合了上述说的向前法和向后法，先用向前发筛选一遍，再用向后法筛选一遍，直到最后无论怎么筛选模型变量都不再发生变化，就算结束了
##### 虚(拟)变量
在多元回归模型中，当遇到数据集是非数值数据类型时，使用分类数据是一个非常有效的方法。分类数据，是指反映(事物)类别的数据，是离散数据，其数据个数(分类属性)有限(但可能很多)且值之间无序。比如，按性别分为男女两类。在一个回归模型中，这些分类值可以用虚拟变量来表示，变量通常取1或者0这样的值，来表示肯定或者否定类型。
##### 虚拟变量圈套
虚拟变量圈套是指两个以上(包括两个)变量之间的高度相关的情形。简而言之，就是存在一个能够被其他变量预测的变量，我们举一个存在重复类别(变量)的直观例子：假设我们舍弃男性类别，那么该类别也可以通过女性类别来定义(女性值为0，表示男性，为1，表示女性)，反之亦然。  
解决虚拟变量圈套的方法是类别变量减去一：假如有m个类别，那么在模型构建时取m-1个虚拟变量，减去的那个变量可以看做是参照值。  
<a href="https://www.codecogs.com/eqnedit.php?latex=D_2=1-D_1" target="_blank"><img src="https://images.gitee.com/uploads/images/2019/0111/152055_11fe091c_1771923.gif" title="D_2=1-D_1" /></a>  
D1、D2虚拟变量  
<a href="https://www.codecogs.com/eqnedit.php?latex=y=b_0&plus;b_1x_1&plus;b_2x_2&plus;b_3D_1" target="_blank"><img src="https://images.gitee.com/uploads/images/2019/0111/152238_f4909b00_1771923.gif" title="y=b_0+b_1x_1+b_2x_2+b_3D_1" /></a>  
##### 1、数据预处理
1. 导入相关库
2. 导入数据集
3. 检查缺失数据
4. 数据分类
5. 编辑虚拟变量并注意避免虚拟变量圈套
6. 特征缩放我们将用简单线性回归模型的相关库来做
##### 2、训练模型
这一步和简单线性回归模型处理相同，用sklearn.liner_model库的LinearRegression类，创建一个LinearRegression类的regressor对象，使用LinearRegression类的fit()方法对数据集进行训练
##### 3、预测结果
在测试集上进行预测，将出输出结果保存在向量Y_pred中，使用上一步训练用到的LinearRegression类的regressor对象，训练完之后用其predict方法预测结果。
参考：[Avik-Jain：100-Days-Of-ML-Code](https://github.com/Avik-Jain/100-Days-Of-ML-Code)  